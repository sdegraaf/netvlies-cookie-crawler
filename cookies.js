phantom.casperPath = './';
phantom.injectJs(phantom.casperPath + 'bin/bootstrap.js'); 

function getLinks() {
	var links = document.querySelectorAll("a");
	var returnLinks = [];
	var blacklist = [
		'jpg','bmp','png','gif','svg','pdf','doc','docx','xls','xlsx','ppt','pptx','rtf','txt','csv','zip','rar','tar','gz','xml','mp3','mp4',
		'wma','flv','mpg','mov','avi','wmv','obj','jar','css','js','rss','fnt','fon','otf','ttf','sit','sitx','dmg','exe','iso','mdf','src',
	];
	Array.prototype.map.call(links, function(e) {
		var href = e.getAttribute("href");
		var ext = href.split('.').pop();
		
		// Filter out blacklisted extensions
		if ( blacklist.indexOf(ext) == -1 ) {
			returnLinks.push(href);
		}
	});
	return returnLinks;
};

function inArray(needle, haystack) {
	var length = haystack.length;
	for(var i = 0; i < length; i++) {
		if(haystack[i] == needle) return true;
	}
	return false;
}

function outputLine(consoleOutput, fileOutput, typeMessage) {
	casper.echo(consoleOutput, typeMessage);
	fs.write(outputFile, fileOutput, 'a');
}

String.prototype.getHostname = function() {
	var re = new RegExp('^(?:f|ht)tp(?:s)?\://([^/]+)', 'im');
	return this.match(re)[1].toString();
}

var casper = require('casper').create({
		//verbose: true,
		//logLevel: "debug",
		javascriptEnabled: true,
		userAgent: 'Mozilla/5.0 (Windows NT 5.1; rv:15.0) Gecko/20100101 Firefox/15.0.1',
		stepTimeout: 600000,
		viewportSize: {
			width: 1024,
			height: 768
		},
		pageSettings: {
			loadImages:  true,
			loadPlugins: true
		}
	}),
	
	fs = require('fs'),
	utils = require('utils'),
	system = require('system'),
	eol = system.os.name == 'windows' ? "\r\n" : "\n",
	
	status = '',
	statusCode = '',
	statusResource = '',

	links = [],
	cookiesFound = [],
	passedUrl = casper.cli.get(0),
	currentCookie,
	debug = (casper.cli.get(1) == 'debug' || casper.cli.get(1) == 'true' ? true : false),
	username = casper.cli.get("user"),
	storedUsername = '',
	
	limit = (casper.cli.get(2) == undefined ? 99999 : casper.cli.get(2)),
	limitCurrent = 0,
	outputFile = './output/cookiecrawler_'+(passedUrl != undefined ? passedUrl.getHostname() : '')+'.txt',
	userFile = './user.txt',
	definitionsFile = './cookie_definitions.csv',
	definitionsObj = {},
	
	httpUser = casper.cli.get("u"),
	httpPass = casper.cli.get("p")
;

// No URL and username passed
if (passedUrl == undefined && username == undefined) {
	casper.echo("Usage: phantomjs cookies.js <website url> [debug] [limit]\nShorthand: ./cookies <website url> [debug] [limit]", 'ERROR').exit();
}
// Username specified, store in file
else if (username != undefined) {
	fs.write(userFile, username, 'w');
	casper.echo("Username saved", 'INFO').exit();
}
// No new username specified, retrieve current username
else {
	if (fs.exists(userFile)) {
		storedUsername = fs.read(userFile);
	}
	else {
		casper.echo('Username file not found. Please use the command ./cookies --user=YOUR_USERNAME', 'ERROR').exit();
	}
}

casper.on("http.status.200", function(resource) {statusResource = resource; statusCode = 200; status = "";});
casper.on("http.status.301", function(resource) {statusResource = resource; statusCode = 301; status = " has been permanently redirected";});
casper.on("http.status.302", function(resource) {statusResource = resource; statusCode = 302; status = " has been temporarily redirected";});
casper.on("http.status.404", function(resource) {statusResource = resource; statusCode = 404; status = " could not be found";});
casper.on("http.status.500", function(resource) {statusResource = resource; statusCode = 500; status = " contains errors";});

// Only show when actually crawling, not when setting username
if (username == undefined) {
	casper.echo('Crawling website'+(limit != 99999 ? ' (limit: '+limit+')' : '')+'... This might take a while depending on the website...', 'INFO');
	fs.write(outputFile, 'Crawling website'+(limit != 99999 ? ' (limit: '+limit+')' : '')+'... This might take a while depending on the website...', 'w');
}

// Get cookie definitions
if (fs.exists(definitionsFile)) {
	var definitionStream = fs.read(definitionsFile);
	definitionStream = definitionStream.split(eol);
	
	for (definition in definitionStream) {
		if (definitionStream[definition].length > 0) {
			var details = definitionStream[definition].split(';');
			definitionsObj[details[0]] = details[1];
		}
	}
}
else {
	casper.echo('Definitions file not found. Descriptions not available', 'ERROR');
}

casper.start();
if (casper.cli.has("u") && casper.cli.has("p")) {
	//casper.setHttpAuth(httpUser, httpPass);
}
//casper.thenOpen('http://cookie-crawler.'+storedUsername+'.nvsotap.nl/crawler/index.php?url='+passedUrl, function() {
//casper.thenOpen('http://33.33.33.10/cookie-crawler/crawler/index.php?url='+passedUrl, function() {
casper.thenOpen('http://www.arcticwebdevelopment.com/crawl/?url='+passedUrl, function() {
	//casper.setHttpAuth(httpUser, httpPass);
	var crawlContents = this.fetchText('body');
	if (debug) {
		this.debugHTML();
	}
	links = crawlContents.split(eol);
	links.splice((links.length-1), 1);
});
casper.then(function() {
	outputLine('Found links:', eol+'Found links:', '');
	for (link in links) {
		outputLine('   '+links[link], eol+'   '+links[link], '');
	}
});
casper.then(function() {
	if (links.length > 0) {
		var foundCookies = 0;
		outputLine(eol+'Retrieving cookies...', eol+eol+'Retrieving cookies...', 'INFO');
		casper.each(links, function(self, link) {
			if (limitCurrent <= limit) {
			    self./*setHttpAuth(httpUser, httpPass).*/thenOpen(link, function() {
			    	if (statusCode == 404 || statusCode == 500) {
			    		outputLine(
							eol+'URL '+(limitCurrent+1)+'/'+links.length+': '+link+status+' - '+statusCode, 
							eol+eol+'URL '+(limitCurrent+1)+'/'+links.length+': '+link+status+' - '+statusCode, 
							'ERROR'
						);
			    		if (debug) {
			    			this.echo('Dump:');
			    			utils.dump(statusResource);
			    		}
			    	}
			    	else {
			    		outputLine(
							eol+'URL '+(limitCurrent+1)+'/'+links.length+': '+link+status+' - '+statusCode, 
							eol+eol+'URL '+(limitCurrent+1)+'/'+links.length+': '+link+status+' - '+statusCode, 
							'INFO'
						);
						if (statusCode != 404 && statusCode != 500) {
							if (debug) {
								this.echo('Cookie dump:');
								utils.dump(phantom.cookies);
							}
							for (i=0; i<phantom.cookies.length; i++) {
								outputLine(
									'   Domain: '+phantom.cookies[i]['domain']+' / Name: '+phantom.cookies[i]['name']+' / Value: '+ phantom.cookies[i]['value'], 
									eol+'   Domain: '+phantom.cookies[i]['domain']+' / Name: '+phantom.cookies[i]['name']+' / Value: '+ phantom.cookies[i]['value'], 
									''
								);
								foundCookies++;
								if (!inArray(phantom.cookies[i]['name'], cookiesFound)) {
									cookiesFound.push(phantom.cookies[i]['name']);
								}
							}
							phantom.clearCookies();
						}
			    	}
					
					if (foundCookies == 0) {
						outputLine('   N/A', eol+'   N/A', '');
					}
					limitCurrent++;
					foundCookies = 0;
				});
			}
			/*
				@todo: Add option to limit the number of results. Sadly, break doesn't seem to work
				else {break;}
			*/
		});
	}
});
casper.run(function() {
	outputLine(eol+'Cookie overview:', eol+eol+'Cookie overview:', 'INFO');
	
	for (found in cookiesFound) {
		outputLine(
			'   '+cookiesFound[found] + (definitionsObj[ cookiesFound[found] ] ? ' ('+definitionsObj[ cookiesFound[found] ]+')' : ''), 
			eol+'   '+cookiesFound[found] + (definitionsObj[ cookiesFound[found] ] ? ' ('+definitionsObj[ cookiesFound[found] ]+')' : ''), 
			''
		);
	}
	
	outputLine(eol+'Finished crawling'+eol, eol+eol+'Finished crawling', 'INFO');
	casper.exit();
});