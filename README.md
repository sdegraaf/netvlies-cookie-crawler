Cookie Crawler commandline usage: ./cookies <website url> [debug] [limit]

Required:
	Website url


Optional:
	Debug (value: debug or true)
	Limit results (int)