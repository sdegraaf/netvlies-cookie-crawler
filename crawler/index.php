<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('memory_limit', '256M');
set_time_limit(0);
include("libs/PHPCrawler.class.php");

ob_implicit_flush(true);

class CookieCrawler extends PHPCrawler {
	public $collected_links = array();
	public function handleDocumentInfo($DocInfo) {
		$lb = "\n";
		
		if ($DocInfo->http_status_code != 401 && $DocInfo->http_status_code != 404 && $DocInfo->http_status_code != 500) {
			$remove_chars = array('"', '>', '<', '(', ')');
			$url = $DocInfo->url;
			$url = strip_tags($url);
			$url = str_replace($remove_chars, "", $url);
			
			// Check if links are concatenated in link (sometimes causes by JS)
			if (substr_count($url, 'http') > 1) {
				$url_exp = explode('http', $url);
				unset($url_exp[0]);
				foreach ($url_exp as $url) {
					if (!in_array($url, $this->collected_links) && !empty($url)) {
						$this->collected_links[] = $url;
						echo 'http'.$url.$lb;
					}
					flush();
				}
			}
			else {
				if (!in_array($url, $this->collected_links) && !empty($url)) {
					$this->collected_links[] = $url;
					echo $url.$lb;
				}
				flush();
			}
		}
		flush();
	}
}

if (isset($_GET['url'])) {
	$crawler = new CookieCrawler();
	$crawler->setURL($_GET['url']);
	//$crawler->addContentTypeReceiveRule("#text/html#");
	$crawler->addURLFilterRule("#\.(jpg|jpeg|bmp|png|gif|svg|pdf|doc|docx|xls|xlsx|ppt|pptx|rtf|txt|csv|zip|rar|tar|gz|xml|mp3|mp4|wma|flv|mpg|mov|avi|wmv|obj|jar|css|js|rss|fnt|fon|otf|ttf|sit|sitx|dmg|exe|iso|mdf|src|src;)$# i");
	$crawler->addURLFilterRule("(.*)\'\+(.*)");
	$crawler->setUserAgentString("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:14.0) Gecko/20100101 Firefox/14.0.1");
	$crawler->addURLFilterRule("['+]");
	$crawler->enableCookieHandling(false);
	$crawler->setUrlCacheType(PHPCrawlerUrlCacheTypes::URLCACHE_SQLITE); 
	//$crawler->setTrafficLimit(1000 * 1024);
	$crawler->go();
	
	if (isset($_GET['debug'])) {
		$report = $crawler->getProcessReport();
		echo "<br />Process runtime: ".$report->process_runtime." sec";
	}
}
?>